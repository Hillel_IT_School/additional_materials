Serenity documentation - http://www.thucydides.info/docs/serenity/
JBehave documentation - https://jbehave.org/
Maven documentation - https://maven.apache.org/
Java 8 lessons - https://www.youtube.com/playlist?list=PL786bPIlqEjSAvjUJtG_q9bFxQnNvI0Ab
Page Object pattern - https://www.pluralsight.com/guides/getting-started-with-page-object-pattern-for-your-selenium-tests
Git lessons - https://githowto.com/ru
Java 8 (Lambda, Stream API intoduction) - https://habr.com/post/216431/
Jenkins Setup instructions - https://www.tutorialspoint.com/jenkins/jenkins_maven_setup.htm
Appium Setup on Androiid Emulator - http://blogs.quovantis.com/appium-basics-part-1-pre-requisites-and-setting-up-your-appium-environment-for-windows/
Appium Setup - https://www.seleniumeasy.com/appium-tutorials/install-appium-on-windows-step-by-step
Appium Setup on real device - https://www.softwaretestinghelp.com/appium-tutorial-how-to-automate-android-apps-on-an-ios-system/

